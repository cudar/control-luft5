# Válvula Proporcional

## Composición del sistema
 ![Valvula compuesta](images/Valvula.png)

 La válvula proporcional es controlada de forma electrónica por medio de una tensión continua de 0V a 12V, en la práctica esta tensión es controlada por medio de PWM denominada _"V2"_. Otras variables a considerar que modifican su comportamiento es la temperatura ambiente, llamada _"T1"_ y la presión de entrada _"P2"_. Dado que _"P2"_ proviene de un tubo exterior se añade la _Reguladora de alta presión_, la misma ofrece a su salida una presión constante, con dicha consideración, la variable no controlada es la temperatura.  
 
 ## Ensayos 

 Idealmente la válvula provee una salida lineal. Sin embargo, dadas las caracteristicas del solenoide, existe un período de efectos transitorios, ocasionados principalmente por el cambio de temperatura en el cobre debido a la corriente que circula por él. Este efecto forma una familia de curvas, simiares a un ciclo de histéresis.

 ![Valvula respuesta](images/valvula_curves_response)

## Objetivo

El Objetivo del desarrollo consiste en generar en un programa en C que por medio de un PWM pueda generar un control exacto y preciso de la válvula, sorteando tanto las variaciones de temperatura como las variaciónes en la salida del caudal.