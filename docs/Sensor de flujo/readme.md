## Sistema de control de flujo

El sistema está constituido por dos partes principales, un Neumotacografo y dos sensores de presión diferencial. Los cuales permiten controlar con precisión el flujo de aire respiratorio.

El Neumotacografo es un transductor, que nos permite medir el índice de flujo de aire, tomando una muestra del caudal dirigido al paciente, y transformando este, a un diferencial de presión que es proporcional al flujo de aire. Este diferencial de presión depende de dos variables, el caudal inyectado y de la densidad. Donde controlar lo mejor posible ambas variables se traduce a una medición del flujo de aire con mayor exactitud. 

Para controlar el caudal se utiliza un transductor neumotacógrafo de orificio variable. Esto nos permite medir la presión proporcional al caudal. El orificio se abre o cierra más o menos dependiendo del flujo de aire, dando así mayor o menor presión.  
 
 ![neuma](images/neumotacografoDeOrificioVariable.png)


Para controlar la densidad, debemos tener en cuenta dos perturbaciones, la temperatura y la presión atmosférica.

Para llevar estos niveles de presión a señales eléctricas, hacemos uso de dos sensores de presión diferencial comerciales. El MP3V5010dp utilizado para las mediciones de alta presión debido a su buena exactitud en estos rangos, y por otro lado el sensor 016mgaa3 que operará a bajas presiones también debido a su mayor exactitud en este rango de presión. 

En la transición de los rangos de mediciones que se realicen entre ambos sensores, se buscará que sea lo más continua posible para que de esta forma las medidas no se vean afectadas significativamente. 

<div align="center">
  <img src="images/TransicionSens12.png"/>
</div>

Estos sensores al ser comerciales, en sus hojas de datos presentan la función de transferencia, que serán los utilizados para realizar el sistema. 
