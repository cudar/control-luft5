//---------------------------------------------librerías----------------------------------------------//
#include "config.h"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "driver/adc.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "freertos/queue.h"
#include "driver/timer.h"
#include "driver/ledc.h"
//----------------------------------------------------------------------------------------------------//

//-----------------------------------------------Declaraciones locales----------------------------------------//
const char *tag = "EVENT";
//pwm:
//int duty = 1000; //0 a 4095
//uart:
static QueueHandle_t uart_queue;

//----------------------------------------------------------------------------------------------------//

//---------------------------------------------configuración GPIO---------------------------------------------//
esp_err_t init_led(void)
{
	gpio_reset_pin(GPIO_PIN_TEST);
	gpio_set_direction(GPIO_PIN_TEST, GPIO_MODE_OUTPUT);
	return ESP_OK;
}
//------------------------------------------------------------------------------------------------------------//

//---------------------------------------------configuración UART---------------------------------------------//
void init_uart(void)
{
    uart_config_t uart_config ={
        .baud_rate = 230400,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };

    uart_param_config(UART_NUM, &uart_config);
   	uart_set_pin(UART_NUM,17,16,UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM, BUFFER_SIZE,BUFFER_SIZE,5,&uart_queue,0);

    ESP_LOGI(tag, "init uart completed");
}
//------------------------------------------------------------------------------------------------------------//

//---------------------------------------------configuración ADC----------------------------------------------//
esp_err_t set_adc(void)
{
	adc1_config_channel_atten(ADC1_CHANNEL_4, ADC_ATTEN_DB_11);
	adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_DB_11);
	adc1_config_width(ADC_WIDTH_BIT_12);
	return ESP_OK;
}
//------------------------------------------------------------------------------------------------------------//

//---------------------------------------------configuración PWM----------------------------------------------//
esp_err_t set_pwm (void)
{
  ledc_channel_config_t channelConfig = { 0 };
  channelConfig.gpio_num = 26;
  channelConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
  channelConfig.channel = LEDC_CHANNEL_0;
  channelConfig.intr_type = LEDC_INTR_DISABLE;
  channelConfig.timer_sel = LEDC_TIMER_0;
  channelConfig.duty = 0;

  ledc_channel_config(&channelConfig);

  ledc_timer_config_t timerConfig = {0};
  timerConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
  timerConfig.duty_resolution = LEDC_TIMER_12_BIT;
  timerConfig.timer_num = LEDC_TIMER_0;
  timerConfig.freq_hz = 10000; //10 khz.

  ledc_timer_config(&timerConfig);

  return ESP_OK;
}

//------------------------------------------------------------------------------------------------------------//

//---------------------------------------------configuración PWM_DUTY----------------------------------------------//
esp_err_t set_pwm_duty(int dutyy)
{
  ledc_set_duty(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0,dutyy);

  ledc_update_duty(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0);

  return ESP_OK;
}

//------------------------------------------------------------------------------------------------------------//

//---------------------------------------------configuración TIMER2--------------------------------------------//
esp_err_t sett_timer(void) 
{
    ESP_LOGI("TIMER", "Inicio de configuración del timer..");

    // Configuración del timer
    timer_config_t config = {
        .divider = TIMER_DIVIDER,					//Configura el divisor del temporizador
        .counter_dir = TIMER_COUNT_UP,				//Define la dirección del contador (aumenta)
        .counter_en = TIMER_PAUSE,					//Pausa el contador al inicio.
        .alarm_en = TIMER_ALARM_EN,					//Habilita la alarma.
        .auto_reload = TIMER_AUTORELOAD_EN,			//Permite que el temporizador se recargue automáticamente al alcanzar la alarma.
    };
    timer_init(TIMER_GROUP_0, TIMER_0, &config);	//Inicializa el temporizador.

    // Configurar el valor inicial del contador a cero:
    timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL);

    // Configurar el valor de la alarma (intervalo):
    timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, TIMER_INTERVAL_US * (TIMER_SCALE / 1000000));

    // Habilitar la interrupción y registrar la ISR
    timer_enable_intr(TIMER_GROUP_0, TIMER_0);
    timer_isr_register(TIMER_GROUP_0, TIMER_0, timer_isr, NULL, ESP_INTR_FLAG_IRAM, NULL);

    // Iniciar el timer
    timer_start(TIMER_GROUP_0, TIMER_0);

    return ESP_OK;
}
//------------------------------------------------------------------------------------------------------------//

