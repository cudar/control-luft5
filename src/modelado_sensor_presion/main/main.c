//---------------------------------------------librerías----------------------------------------------//
#include "config.h"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "driver/adc.h"
#include "driver/uart.h"
#include "freertos/queue.h"
#include "driver/timer.h"
#include "driver/ledc.h"
//----------------------------------------------------------------------------------------------------//

//--------------------------------------Variables locales---------------------------------------------//
//adc:
int adc_val=0;
int adc_val2=0;
uint8_t contador_global=0;
//log: 
static const char* TAG = "MyModule";
//pwm: 
int duty=0;
//tabla senoidal:
#define SINE_TABLE_SIZE 100               // Número de muestras para la tabla senoidal
uint16_t sine_table[SINE_TABLE_SIZE];  // Tabla senoidal de 100 muestras para el pwm

float test1;
float test2;

//----------------------------------------------------------------------------------------------------//

//-----------------------------------------callback del timer2-----------------------------------------//
void IRAM_ATTR timer_isr(void *param) {		//función que se almacena en la mem de alta velocidad para ejecución rápida.
	
//union para enviar datos de adc1 por uart:
	union 
  {
    float valor_float;
    uint8_t bytes[sizeof(float)];
  } floatUnion;
  	//union para enviar datos de adc2 por uart:
  union 
  {
    float valor_float2;
    uint8_t bytes2[sizeof(float)];
  } floatUnion2;

   uint8_t trama[10];  // Tamaño total de la trama

static int index = 0;                           // Índice para recorrer la tabla senoidal

duty = sine_table[index];                       
set_pwm_duty(duty);                             // Actualizar el ciclo de trabajo del PWM
index = (index + 1) % SINE_TABLE_SIZE;          // Incrementar el índice y hace un bucle

  	//Lectura de los dos adc:
	adc_val=adc1_get_raw(ADC1_CHANNEL_4);
	adc_val2=adc1_get_raw(ADC1_CHANNEL_5);
	
	//cast al rango 0-3.3 de los dos adc:
	floatUnion.valor_float = adc_val*0.0008056640625;
	floatUnion2.valor_float2 = adc_val2*0.0008056640625;
	test1=floatUnion.valor_float;
    test2=floatUnion2.valor_float2;
  trama[0] = 0xA;
 
  for (int i = 0; i < 4; i++) {
        trama[2 + i] = floatUnion.bytes[i];
    }
      for (int i = 0; i < 4; i++) {
        trama[6 + i] = floatUnion2.bytes2[i];
    }
  trama[1] = contador_global;  
  contador_global ++;
  uart_write_bytes(UART_NUM,(const char*) trama, sizeof(trama));

  ESP_LOGI(TAG,"adc1: %f ", test1);
  ESP_LOGI(TAG,"adc2: %f ", test2);
	//test para ver si realmente funciona a 3KHz (medir en el osciloscopio, debería ver una frec. de 1.5KHz):
	gpio_set_level(GPIO_PIN_TEST, !gpio_get_level(GPIO_PIN_TEST));

    // Limpiar la interrupción y rearma la alarma del timer:
  timer_group_clr_intr_status_in_isr(TIMER_GROUP_0, TIMER_0);	//Limpia el estado de la interrupción para que se pueda reactivar.
  timer_group_enable_alarm_in_isr(TIMER_GROUP_0, TIMER_0);	//Reactiva la alarma del temporizador para que vuelva a dispararse tras el time definido.
}
//----------------------------------------------------------------------------------------------------//

//-----------------------------------------------función MAIN-------------------------------------------------//
void app_main(void)
{
    set_pwm();
    init_led();
    init_uart();
    set_adc();
    create_sine_table();
    vTaskDelay(pdMS_TO_TICKS(5000));              // delay 5 seg. antes de iniciar las rutinas y lecturas.
    sett_timer();
}
//------------------------------------------------------------------------------------------------------------//

//---------------------------------------------crea tabla senoidal--------------------------------------------//
void create_sine_table(void)
{
    for (int i = 0; i < SINE_TABLE_SIZE; i++)
    {
        sine_table[i] = (uint16_t)((sin(2 * M_PI * i / SINE_TABLE_SIZE) + 1) * 2047.5);  //mapea los 100 valores de 0 a 4095 para el pwm. 
    }
}
//------------------------------------------------------------------------------------------------------------//