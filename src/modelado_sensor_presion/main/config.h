//---------------------------------------------librerías----------------------------------------------//
#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "driver/adc.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "freertos/queue.h"
#include "driver/timer.h"
#include "driver/ledc.h"
#include <math.h> 
//----------------------------------------------------------------------------------------------------//

//---------------------------------------------prototipos---------------------------------------------//
void init_uart(void);
esp_err_t set_pwm(void);
esp_err_t set_pwm_duty(int);
esp_err_t set_adc(void);
esp_err_t sett_timer(void);
esp_err_t init_led(void);
void IRAM_ATTR timer_isr(void *);
void create_sine_table(void);
//----------------------------------------------------------------------------------------------------//

//-------------------------------------------declaraciones-------------------------------------------//
	
//timer2:
#define TIMER_DIVIDER 16  // Divisor del timer para reducir la frecuencia del reloj
#define TIMER_BASE_CLK (80 * 1000000)  // 80 MHz
#define TIMER_SCALE (TIMER_BASE_CLK / TIMER_DIVIDER)  // Convertir la frecuencia del reloj a microsegundos
#define TIMER_INTERVAL_US (1000000 / 100)  // Intervalo para 2 kHz (xx microsegundos)
#define GPIO_PIN_TEST 5

//uart: 
#define UART_NUM UART_NUM_2  
#define BUFFER_SIZE 1024
#define TASK_MEMORY 2048




//----------------------------------------------------------------------------------------------------//

#endif 