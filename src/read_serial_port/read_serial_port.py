import serial
import struct
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import threading
import time

ser = serial.Serial('COM6', 230400, timeout=1)  #configuración puerte serie.

# Listas para almacenar los datos
times = []
adc1_vals = []
adc2_vals = []

# Función para leer datos del puerto serie en un hilo separado
def read_serial():
    global times, adc1_vals, adc2_vals
    start_time = time.time()
    
    while True:
        trama = ser.read(10)
        if len(trama) == 10 and trama[0] == 0xA:
            contador = trama[1]
            adc1_valor = struct.unpack('f', trama[2:6])[0]
            adc2_valor = struct.unpack('f', trama[6:10])[0]

            # Guardar en el archivo
            with open("datos.txt", 'a') as archivo:
                archivo.write(f"{contador}\t{adc1_valor}\t{adc2_valor}\n")

            # Agregar valores al gráfico
            times.append(time.time() - start_time)
            adc1_vals.append(adc1_valor)
            adc2_vals.append(adc2_valor)

            # Limitar la longitud de los datos para no sobrecargar la gráfica
            if len(times) > 100:
                times.pop(0)
                adc1_vals.pop(0)
                adc2_vals.pop(0)

# Hilo de lectura del puerto serie
thread = threading.Thread(target=read_serial, daemon=True)
thread.start()

# Configuración de la gráfica
fig, ax = plt.subplots()
line1, = ax.plot([], [], label='ADC1')
line2, = ax.plot([], [], label='ADC2')
ax.legend()

def update_plot(frame):
    line1.set_data(times, adc1_vals)
    line2.set_data(times, adc2_vals)
    ax.relim()
    ax.autoscale_view()
    return line1, line2

# Animación en matplotlib
ani = animation.FuncAnimation(fig, update_plot, blit=True)
plt.show()
