//librerías:
#include <stdio.h>
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/timer.h"
#include "driver/ledc.h"
#include "driver/dac.h"
#include <rom/ets_sys.h>
#include <math.h> 
#define SINE_TABLE_SIZE 100               // Número de muestras para la tabla senoidal

//prototipos:
esp_err_t set_pwm(void);
esp_err_t set_pwm_duty(int);
void init_dac(void);
void rutina3(void);
void create_sine_table(void);

//incializaciones:
int duty = 0;                           //0 a 4095
int dac_val = 0;                        //0 a 255

uint16_t sine_table[SINE_TABLE_SIZE];  // Tabla senoidal de 100 muestras para el pwm
uint8_t sine_table2[SINE_TABLE_SIZE];  // Tabla senoidal de 100 muestras para el dac


//función principal:
void app_main(void)
{
    set_pwm();
    set_pwm_duty(duty);
    init_dac();
    create_sine_table();
    while (1)
    {
      rutina3();
    }
}

void rutina3(void)
{
static int index = 0;                           // Índice para recorrer la tabla senoidal

duty = sine_table[index];                       
dac_val=sine_table2[index];
set_pwm_duty(duty);                             // Actualizar el ciclo de trabajo del PWM


index = (index + 1) % SINE_TABLE_SIZE;          // Incrementar el índice y hace un bucle

dac_output_voltage(DAC_CHANNEL_1, dac_val);     // enviar valor al DAC

//vTaskDelay(pdMS_TO_TICKS(10));              // delay 10 ms. esta fnc define la frecuencia de la senoidal.  
ets_delay_us(10);                             //delay de 10 us. 500 hz.  
}

void create_sine_table(void)
{
    for (int i = 0; i < SINE_TABLE_SIZE; i++)
    {
        sine_table[i] = (uint16_t)((sin(2 * M_PI * i / SINE_TABLE_SIZE) + 1) * 2047.5);  //mapea los 100 valores de 0 a 4095 para el pwm. 
        sine_table2[i] = (uint8_t)((sin(2 * M_PI * i / SINE_TABLE_SIZE) + 1) * 127.5);  //mapea los 100 valores de 0 a 255 para el dac.
    }
}

//incialiación del dac:
void init_dac(void) 
{
    dac_output_enable(DAC_CHANNEL_1);       //inicializo el DAC en el GPIO25. Para el gpio26: 'DAC_CHANNEL_2'
}

//configuración pwm:
esp_err_t set_pwm(void)
{
  ledc_channel_config_t channelConfig = { 0 };
  channelConfig.gpio_num = 26;
  channelConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
  channelConfig.channel = LEDC_CHANNEL_0;
  channelConfig.intr_type = LEDC_INTR_DISABLE;
  channelConfig.timer_sel = LEDC_TIMER_0;
  channelConfig.duty = 0;

  ledc_channel_config(&channelConfig);

  ledc_timer_config_t timerConfig = {0};
  timerConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
  timerConfig.duty_resolution = LEDC_TIMER_12_BIT;
  timerConfig.timer_num = LEDC_TIMER_0;
  timerConfig.freq_hz = 10000; //10 khz.

  ledc_timer_config(&timerConfig);

  return ESP_OK;  
}

//seteo del duty cycle pwm: 
esp_err_t set_pwm_duty(int duty)
{
  ledc_set_duty(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0,duty);
  ledc_update_duty(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0);
  return ESP_OK;
}
