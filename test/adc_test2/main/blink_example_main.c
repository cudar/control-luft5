#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "led_strip.h"
#include "sdkconfig.h"
#include "driver/ledc.h"
#include "driver/adc.h"


#define led1 1

int timerId=1;
int interval=20;
int adc_val=0;
int adc_val2=0;


uint8_t led_level=0;
TimerHandle_t xTimers;
static const char *tag ="Main";

esp_err_t init_led(void);
esp_err_t blink_led(void);
esp_err_t set_timer(void);
esp_err_t set_adc(void);


void vTimerCallback(TimerHandle_t pxTimer)
{

	adc_val=adc1_get_raw(ADC1_CHANNEL_4);
	adc_val2=adc1_get_raw(ADC1_CHANNEL_5);
	printf("adc: %d\n", adc_val);
	printf("adc2: %d\n", adc_val2);

}


void app_main(void)
{
	set_adc();
	set_timer();

}

esp_err_t set_timer(void)
{
	ESP_LOGE(tag,"Timer init configuration");
	xTimers= xTimerCreate("Timer",             // Just a text name, not used by the kernel.
	                     (pdMS_TO_TICKS(interval)), // The timer period in ticks.
	                      pdTRUE,              // The timers will auto-reload themselves when they expire.
	                     ( void * )timerId,        // Assign each timer a unique id equal to its array index.
	                      vTimerCallback       // Each timer calls the same callback when it expires.
	                                      );

	          if( xTimers== NULL )
	          {
	              // The timer was not created.
	        	  ESP_LOGE(tag,"The timer was not created");
	          }
	          else
	          {
	              if( xTimerStart( xTimers, 0 ) != pdPASS )
	              {
	            	  ESP_LOGE(tag,"The timer was not created");
	                  // The timer could not be set into the Active state.
	              }
	          }

	return ESP_OK;
}

esp_err_t init_led(void)
{
	gpio_reset_pin(led1);
	gpio_set_direction(led1,GPIO_MODE_OUTPUT);

	return ESP_OK;
}

esp_err_t blink_led(void)
{
	led_level=!led_level;
	gpio_set_level(led1,led_level);
	return ESP_OK;
}

esp_err_t set_adc(void)
{
	adc1_config_channel_atten(ADC1_CHANNEL_4, ADC_ATTEN_DB_11);
	adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_DB_11);
	adc1_config_width(ADC_WIDTH_BIT_10);
	return ESP_OK;
}
