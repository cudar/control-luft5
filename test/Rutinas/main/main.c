//librerías:
#include <stdio.h>
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/timer.h"
#include "driver/ledc.h"
#include "driver/dac.h"

//prototipos:
esp_err_t set_pwm(void);
esp_err_t set_pwm_duty(int);
void rutina1 (int,int);
void rutina2 (int,int);
void init_dac(void);

//incializaciones:
int duty = 0;                        //0 a 4095
int dac_val = 0;                    //0 a 255

//función principal:
void app_main(void)
{
    set_pwm();
    set_pwm_duty(duty);
    init_dac();
    while (1)
    {
      rutina2(4095,255);
    }
    rutina1(5,5);
     
}

//función rutina-escalonada:
void rutina1 (int steps_pwm, int steps_dac)                               
{
    int incremento_pwm = 4095 / steps_pwm;                       
    int incremento_dac = 255/steps_dac;

    for (int i = 0; i < steps_dac; i++) 
    {
        duty = i * incremento_pwm;                      // incremento el duty cycle
        dac_val = i * incremento_dac;                   // incremento el valor del dac
        dac_output_voltage(DAC_CHANNEL_1,dac_val);      // enviar valor al DAC
        set_pwm_duty(duty);                             // actualizo el duty cycle
        vTaskDelay(pdMS_TO_TICKS(1000));                // delay 1seg
    }
}

//función rutina-diente_de_sierra:
void rutina2 (int steps_pwm, int steps_dac)                                
{
    for (int i = 0; i < 255; i++) 
      {
        //duty = (4095 * i) / steps_pwm;                  // calcular el duty en base a la resolución de 12 bits
        dac_val = (255 * i) / steps_dac;                // dac de 8 bits
        dac_output_voltage(DAC_CHANNEL_1, dac_val);     // enviar valor al DAC
        //set_pwm_duty(duty);                             // función que ajusta el duty cycle
        vTaskDelay(pdMS_TO_TICKS(10));                  // delay entre cada incremento
      }
    //reinicio a cero: 
    dac_output_voltage(DAC_CHANNEL_1, 0);
    set_pwm_duty(0);                                    // función que ajusta el duty cycle
    vTaskDelay(pdMS_TO_TICKS(10));                      // delay 10ms
}

//incialiación del dac:
void init_dac(void) 
{
    dac_output_enable(DAC_CHANNEL_1);   //inicializo el DAC en el GPIO25. Para el gpio26: 'DAC_CHANNEL_2'
}

//configuración pwm:
esp_err_t set_pwm(void)
{
  ledc_channel_config_t channelConfig = { 0 };
  channelConfig.gpio_num = 26;
  channelConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
  channelConfig.channel = LEDC_CHANNEL_0;
  channelConfig.intr_type = LEDC_INTR_DISABLE;
  channelConfig.timer_sel = LEDC_TIMER_0;
  channelConfig.duty = 0;

  ledc_channel_config(&channelConfig);

  ledc_timer_config_t timerConfig = {0};
  timerConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
  timerConfig.duty_resolution = LEDC_TIMER_12_BIT;
  timerConfig.timer_num = LEDC_TIMER_0;
  timerConfig.freq_hz = 10000; //10 khz.

  ledc_timer_config(&timerConfig);

  return ESP_OK;  
}

//seteo del duty cycle pwm: 
esp_err_t set_pwm_duty(int duty)
{
  ledc_set_duty(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0,duty);
  ledc_update_duty(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0);
  return ESP_OK;
}
